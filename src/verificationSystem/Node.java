package verificationSystem;

public class Node
{
	String expiredReason="";
	Boolean expiredStatus;
	String networkStatus="";	
	
	public String getNetworkStatus() {
		return networkStatus;
	}

	public void setNetworkStatus(String networkStatus) {
		this.networkStatus = networkStatus;
	}

	public String getExpiredReason() 
	{
		return expiredReason;
	}

	public void setExpiredReason(String expiredReason) 
	{
		this.expiredReason = expiredReason;
	}

	public Boolean getExpiredStatus() {
		return expiredStatus;
	}

	public void setExpiredStatus(Boolean expiredStatus) {
		this.expiredStatus = expiredStatus;
	}
	
}
