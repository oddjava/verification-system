package verificationSystem;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.jsoup.Connection.Response;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;

import mongoDBConnection.GetConnection;

public class Before45Days {
	static MongoClient mongoClient;
	static DB db;
	static DBCollection coll;
	static DBCollection expiredCollection;
	static int count;

	Calendar calender;
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");;

	Date currentDate;
	String todayDate;

	BasicDBObject andQuery;
	List<BasicDBObject> obj;
	DBCursor cursor;
	DBObject present;

	boolean redirectStatus;
	Node node, redirectedNode;

	int depth1, depth2;

	ArrayList<Data45> list = new ArrayList<>();
	Data45 data;

	Calendar cal=null;
	
	Date date;
	
	private ExecutorService executor;
	private MyRunnable45 worker;
	static {

		coll = GetConnection.connect("sampleCrawldata");
		expiredCollection = GetConnection.connect("expiredCollection");
		count = 1;
	}

	public static void main(String[] args) {
		new Before45Days().last15days();
	}

	public void last15days() {

		date=new Date();
		cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, -16);			
		date = cal.getTime();
		String dateBefore16Days=formatter.format(date);

		cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, -30);			
		date = cal.getTime();	 
		String dateBefore45Days=formatter.format(date);


		date = new Date();		
		todayDate = formatter.format(date);//Today's Date

		System.out.println("Before 16 days date::" + dateBefore16Days);
		System.out.println("Before 45 days date::" + dateBefore45Days);

		andQuery = new BasicDBObject();
		obj = new ArrayList<BasicDBObject>();
		obj.clear();
		obj.add(new BasicDBObject("status", "true"));
		obj.add(new BasicDBObject("expiredStatus", "live"));
		obj.add(new BasicDBObject("systemDate", new BasicDBObject("$gte", dateBefore16Days).append("$lt", dateBefore45Days)));
		andQuery.put("$and", obj);
		cursor = coll.find(andQuery);
		System.out.println("Total number of records fethched by query" + cursor.count());
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);
		String link;

		while (cursor.hasNext()) {
			present = cursor.next();
			link = present.get("link").toString();
			String authorityTitle;
			try {
				authorityTitle = present.get("authorityTitle").toString();
				if (authorityTitle.equals(null)) {
					authorityTitle = present.get("title").toString();
					System.out.println("Authority title is NULL");

				}

			} catch (NullPointerException e) {
				authorityTitle = present.get("title").toString();
			}

			data = new Data45(link, authorityTitle);
			list.add(data);

			if (list.size() == 100) {
				System.out.println("List is 100 and calling Threads");
				intiateThreads(list);
				list.clear();
			}
		}

		if (list.size() > 0) {
			System.out.println("Executing final batch");
			intiateThreads(list);
			list.clear();
		}
		System.out.println("Done with Execution");

	}

	public void intiateThreads(ArrayList<Data45> list) {
		// TODO Auto-generated method stub
		executor = Executors.newFixedThreadPool(100);
		for (int i = 0; i < list.size(); i++) {
			worker = new MyRunnable45(list.get(i).getLink(), list.get(i).getAuthorityTitle(), todayDate, coll,
					expiredCollection, present);
			executor.execute(worker);
		}
		executor.shutdown();
		// Wait until all threads are finish
		while (!executor.isTerminated()) {
			// System.out.println("Executer termination
			// "+executor.isTerminated());
		}
	}

	public Node getStatus(String authorityTitle, String link) {
		Document doc;
		Response response;
		String redirectedLink;
		boolean status = true;
		node = new Node();
		String title;
		try {
			if(!link.startsWith("http"))
				link = "http://"+link;
			response = Jsoup.connect(link).header("Accept-Language", "en").timeout(30000).followRedirects(false)
					.execute();
			doc = response.parse();

			try {
				if (response.hasHeader("location"))
					redirectedLink = response.header("location");
				else
					redirectedLink = link;
			} catch (NullPointerException e) {
				redirectedLink = link;
			}

			if (!(link.contains("twitter.com") || link.endsWith(".pdf"))) {
				
				status = expireMessage(doc); // Expiry check based on expire
												// message

				if (status == false) {
					System.err.println("Expired due to EXPIRY MESSAGE " + link);
					status = false;
					node.setExpiredReason("Jsoup Doc contains Job not available strings");
					node.setExpiredStatus(status);
					return node;
				}

				try {
					title = doc.title(); // Doc title is NULL means link is
											// expired.
				} catch (NullPointerException e) {
					System.err.println("Expired due to Title is NULL" + link);
					status = false;
					node.setExpiredReason("Title is null on html doc");
					node.setExpiredStatus(status);
					return node;
				}

				status = titleComparision(authorityTitle, title);
				if (status == false) {
					System.err.println("Link is expired due to TITLE NOT MATCHED" + link);
					status = false;
					node.setExpiredReason("Authority title & title does not match");
					node.setExpiredStatus(status);
					return node;
				}

				if(link.replaceAll("https?://", "").equals(redirectedLink.replaceAll("https?://", ""))){
					node.setExpiredReason("Link redirected so false");
					node.setExpiredStatus(status);
					return node;
				}

				int depth1 = getdepth(link);
				int depth2 = getdepth(redirectedLink);

				/*
				 * if (depth1 == depth2) { node.setExpiredStatus("true"); return
				 * node; }
				 */
				if (depth1 != depth2) {
					node.setExpiredReason("Depth of url and redirected url are not same");
					node.setExpiredStatus(status);
					return node;
				}

				int statusCode = response.statusCode();

				status = checkonStatus(statusCode);

				if (status == false) {
					node.setExpiredReason("HTTP status code is "+statusCode);
					node.setExpiredStatus(status);
					return node;
				}

				if (status == true) {
					node.setExpiredStatus(status);
					return node;
				}
			} else {
				status = true;
				node.setExpiredStatus(status);
				return node;
			}
		}catch (MalformedURLException e) {
			System.err.println(link+" is Malformed");
			status = false;
			node.setExpiredReason("Malformed URL");
			node.setExpiredStatus(status);
			return node;
		}
		catch (UnknownHostException e) {
			System.out.println("Code exiting due to Net problem");
			System.exit(0);
			status = true;
			node.setExpiredStatus(status);
			return node;
		} catch (NullPointerException e) {
			status = false;
			node.setExpiredReason("Null Pointer Exception");
			node.setExpiredStatus(status);
			return node;
		} catch (HttpStatusException e) {
			System.err.println("Expired Link due to HttpStatus Exception:::" + link);
			status = false;
			node.setExpiredReason("HTTP status Exception");
			node.setExpiredStatus(status);
			return node;
		} catch (UnsupportedMimeTypeException e) {
			status = true;
			node.setExpiredStatus(status);
			return node;
		} catch (Exception e) {
			System.out.println("Live Link:::" + link);
			System.out.println("Other Exception" + e.getMessage());
			status = true;
			node.setExpiredStatus(status);
			return node;
		}
		node.setExpiredStatus(true);
		return node;
	}

	public boolean titleComparision(String authorityTitle, String title) {
		// TODO Auto-generated method stub.

		title = title.trim();
		authorityTitle = authorityTitle.trim();
		if (authorityTitle.contentEquals(title)) {
			return true;
		}
		title = title.replaceAll("[^\\w\\s]", " ").replaceAll(" +", " ").trim().toLowerCase();
		authorityTitle = authorityTitle.replaceAll("[^\\w\\s]", " ").replaceAll(" +", " ").trim().toLowerCase();

		String[] dbtitle = title.split(" +");
		authorityTitle = " " + authorityTitle + " ";
		int cnt = 0;
		for (int i = 0; i < dbtitle.length; i++) {
			if (!authorityTitle.contains(" " + dbtitle[i] + " ")) {
				cnt++;
			}
		}

		if (cnt > 3) {
			return false;
		} else {
			return true;
		}
	}

	private boolean checkonStatus(int statusCode) {

		if (statusCode == 200) {
			return true;
		}
		return false;
	}

	public int getdepth(String url) {
		// TODO Auto-generated method stub
		int depth = 0;

		for (int i = 0; i < url.length(); i++) {
			char s = url.charAt(i);
			if (s == '/') {
				depth++;
			}
		}
		return depth;
	}

	public boolean expireMessage(Document doc) {
		if (doc.text().contains("Sorry this Ad is no longer available")
				|| doc.text().contains("This Job is no longer available")
				|| doc.text().contains("Sorry, that page doesn�t exist!") || doc.text().contains("job not found")
				|| doc.text().contains("job does not exist") || doc.text().contains("Job listing not found")
				|| doc.text().contains("The job listing you were looking for doesn�t seem to exist anymore.")
				|| doc.text().contains("The Job You Selected Has Expired")
				|| doc.text().contains("Sorry! That job has been removed")
				|| doc.text().contains("The Job You Selected Has Expired")
				|| doc.text().contains("Job listing not found")
				|| doc.text().contains("Sorry, we couldn't find the page you're looking for.")
				|| doc.text().contains("Sorry! This position is not active yet or has been expired.")
				|| doc.text().contains("This job has been closed") || doc.text().contains("Sorry. The COW says 404")
				|| doc.text().contains("We will be back soon"))

		{

			return false;

		} else {
			return true;
		}

	}

}

class MyRunnable45 implements Runnable {

	private String link, authorityTitle, todayDate;
	boolean status;
	BasicDBObject updateFields;
	BasicDBObject setQuery;
	DBCollection coll, expiredCollection;
	DBObject present;

	public MyRunnable45(String link, String authorityTitle, String todayDate, DBCollection coll,
			DBCollection expiredCollection, DBObject present) {
		// TODO Auto-generated constructor stub
		this.link = link;
		this.authorityTitle = authorityTitle;
		this.todayDate = todayDate;
		this.coll = coll;
		this.expiredCollection = expiredCollection;
		this.present = present;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub

		Node node = new Before45Days().getStatus(authorityTitle, link);

		status = node.getExpiredStatus();

		if (status == false) {
			
			updateFields = new BasicDBObject();
			updateFields.append("expiredStatus", "expired");
			updateFields.append("expiredDate", todayDate);
			updateFields.append("expiredReason", node.getExpiredReason());
			setQuery = new BasicDBObject();
			setQuery.append("$set", updateFields);
			coll.update(present, setQuery);
			expiredCollection.insert(new BasicDBObject("link", link).append("expiredDate", todayDate));
		} else {
			System.out.println(link + " is live");
		}

	}

}

class Data45 {

	private String link;
	private String authorityTitle;

	public Data45(String link, String authorityTitle) {
		super();
		this.link = link;
		this.authorityTitle = authorityTitle;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getAuthorityTitle() {
		return authorityTitle;
	}

	public void setAuthorityTitle(String authorityTitle) {
		this.authorityTitle = authorityTitle;
	}

}