package verificationSystem;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.UnsupportedMimeTypeException;
import org.jsoup.Connection.Response;
import org.jsoup.nodes.Document;

import com.load.LoadAvg;
import com.load.LoadException;
import com.mongodb.BasicDBObject;
import com.mongodb.Bytes;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoException;
import com.mongodb.MongoSocketOpenException;
import com.mongodb.MongoSocketWriteException;

import mongoDBConnection.GetConnection;

public class NoThread0_15 {

	static MongoClient mongoClient;
	static DB db;
	static DBCollection coll;
	static DBCollection expiredCollection;
	static int done = 0;

	Calendar calender;
	SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");;

	Date currentDate;
	String todayDate;
	String dateBefore15Days;

	BasicDBObject andQuery;
	List<BasicDBObject> obj;
	DBCursor cursor;
	DBObject present;

	static List<BasicDBObject> subCategories;

	boolean redirectStatus;
	Node node;

	int depth1, depth2;
	boolean status;
	BasicDBObject updateFields;
	BasicDBObject setQuery;

	LoadAvg load = new LoadAvg();
	URL url;
	HttpURLConnection connection;

	static {

		subCategories = new ArrayList<>();

		subCategories.add(new BasicDBObject("subCategory", "Classifieds"));
		subCategories.add(new BasicDBObject("subCategory", "Forums"));
		subCategories.add(new BasicDBObject("subCategory", "RemoteWork"));
		subCategories.add(new BasicDBObject("subCategory", "Marketplace"));
		subCategories.add(new BasicDBObject("subCategory", "BusinessOpportunities"));
		subCategories.add(new BasicDBObject("subCategory", "Tenders"));
		subCategories.add(new BasicDBObject("subCategory", "Social"));
		subCategories.add(new BasicDBObject("subCategory", "Sellers"));
		subCategories.add(new BasicDBObject("subCategory", "Signals"));
		subCategories.add(new BasicDBObject("subCategory", "BlogArticles"));
		subCategories.add(new BasicDBObject("subCategory", "Journalist"));
		subCategories.add(new BasicDBObject("subCategory", "PrOpportunities"));
		subCategories.add(new BasicDBObject("subCategory", "Franchise"));
		subCategories.add(new BasicDBObject("subCategory", "Events"));
		subCategories.add(new BasicDBObject("subCategory", "Influencers"));
		subCategories.add(new BasicDBObject("subCategory", "OnlineCommunities"));
		subCategories.add(new BasicDBObject("subCategory", "Webinar"));
	}

	public static void main(String[] args) {
		coll = GetConnection.connect("sampleCrawldata", args[0]);
		expiredCollection = GetConnection.connect("expiredCollection", args[0]);
		new NoThread0_15().last0_15days();
	}

	public void last0_15days() {
		String startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date());
		System.out.println(startTime);

		currentDate = new Date();
		calender = Calendar.getInstance();
		calender.setTime(currentDate);
		calender.add(Calendar.DATE, -15);
		currentDate = calender.getTime();
		dateBefore15Days = formatter.format(currentDate);

		currentDate = new Date();
		todayDate = formatter.format(currentDate);

		System.out.println("Today's Date::" + todayDate);
		System.out.println("Date before 15 days::" + dateBefore15Days);

		andQuery = new BasicDBObject();

		obj = new ArrayList<BasicDBObject>();
		obj.clear();

		obj.add(new BasicDBObject("status", "true"));
		obj.add(new BasicDBObject("expiredStatus", "live"));
		obj.add(new BasicDBObject("systemDate", new BasicDBObject("$gte", dateBefore15Days).append("$lt", todayDate)));
		andQuery.put("$and", obj);
		andQuery.put("$or", subCategories);

		System.out.println(andQuery);
		cursor = coll.find(andQuery);
		System.out.println("Total number of records fethched by query" + cursor.count());
		cursor.addOption(Bytes.QUERYOPTION_NOTIMEOUT);

		String link, moduleName;

		try {
			if (load.getLoadAvg() >= 1.5) {
				throw new LoadException();
			} else {

				while (cursor.hasNext()) {
					present = cursor.next();
					link = present.get("link").toString();
					String authorityTitle;
					try {
						authorityTitle = present.get("authorityTitle").toString();
						if (authorityTitle.equals(null)) {
							authorityTitle = present.get("title").toString();
							System.out.println("Authority title is NULL");

						}

					} catch (NullPointerException e) {
						authorityTitle = present.get("title").toString();
					}
					moduleName = present.get("moduleName").toString();

					node = new NoThread0_15().getStatus(authorityTitle, link, moduleName);

					status = node.getExpiredStatus();

					if (status == false) {
						updateFields = new BasicDBObject();
						updateFields.append("expiredStatus", "expired");
						updateFields.append("expiredDate", todayDate);
						updateFields.append("expiredReason", node.getExpiredReason());
						setQuery = new BasicDBObject();
						setQuery.append("$set", updateFields);
						coll.update(present, setQuery);
						expiredCollection.insert(new BasicDBObject("link", link).append("expiredDate", todayDate));
					} else {
						System.out.println(link + " is live");
					}
					done = ++done;
					System.out.println("Done for " + done);

				}
			}
		} catch (LoadException e) {

		}

		System.out.println("Done with Execution");
		String endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm a").format(new Date());

		System.out.println("Start time was " + startTime);
		System.out.println("End time was " + endTime);
	}

	public Node getStatus(String authorityTitle, String link, String moduleName) {

		Document doc;
		Response response;
		String redirectedLink;
		boolean status = true;
		Node node = new Node();
		String title;
		try {

			if (load.getLoadAvg() >= 1.5) {
				throw new LoadException();
			}
			if (!link.startsWith("http"))
				link = "http://" + link;

			if (load.getLoadAvg() >= 1.5) {
				throw new LoadException();
			}

			url = new URL(link);
			connection = (HttpURLConnection) url.openConnection();
			connection.setRequestMethod("GET");
			connection.connect();

			int statusCode = connection.getResponseCode();

			// status = checkonStatus(statusCode);

			switch (statusCode) {
			case 403:
				node.setExpiredStatus(true);
				return node;
			case 404:
				node.setExpiredReason("Expired due to 404");
				node.setExpiredStatus(false);
				return node;
			case 410:
				node.setExpiredReason("Expired due to 410");
				node.setExpiredStatus(false);
				return node;
			case 429:
				node.setExpiredStatus(true);
				return node;
			case 451:
				node.setExpiredStatus(true);
				return node;
			case 500:
				node.setExpiredReason("Expired due to 500");
				node.setExpiredStatus(false);
				return node;
			case 501:
				node.setExpiredReason("Expired due to 501");
				node.setExpiredStatus(false);
				return node;
			case 503:
				node.setExpiredReason("Expired due to 503");
				node.setExpiredStatus(false);
				return node;
			case 505:
				node.setExpiredStatus(true);
				return node;
			}

			/*
			 * if (status == false) { System.err.println(
			 * "Link expire due to HTTP status "+link); System.err.println(
			 * "HTTP status code is "+statusCode); node.setExpiredReason(
			 * "HTTP status code is " + statusCode);
			 * node.setExpiredStatus(status); return node; }
			 */

			response = Jsoup.connect(link).header("Accept-Language", "en").timeout(30000).followRedirects(false)
					.execute();

			doc = response.parse();

			if (load.getLoadAvg() >= 1.5) {
				throw new LoadException();
			}
			try {
				if (response.hasHeader("location"))
					redirectedLink = response.header("location");
				else
					redirectedLink = link;
			} catch (NullPointerException e) {
				redirectedLink = link;
			}

			if (load.getLoadAvg() >= 1.5) {
				throw new LoadException();
			}
			if (!(link.contains("twitter.com") || link.endsWith(".pdf"))) {

				status = expireMessage(doc); // Expiry check based on expire
												// message

				if (status == false) {
					System.err.println("Expired due to EXPIRY MESSAGE " + link);
					status = false;
					node.setExpiredReason("Jsoup Doc contains Job not available strings");
					node.setExpiredStatus(status);
					return node;
				}

				try {
					title = doc.title().trim(); // Doc title is NULL means link
												// is
					// expired.
					if (title.isEmpty()) {
						System.err.println("Expired due to Title is NULL" + link);
						status = false;
						node.setExpiredReason("Title is null on html doc");
						node.setExpiredStatus(status);
						return node;
					}
				} catch (NullPointerException e) {
					System.err.println("Expired due to Title is NULL" + link);
					status = false;
					node.setExpiredReason("Title is null on html doc");
					node.setExpiredStatus(status);
					return node;
				}

				if (load.getLoadAvg() >= 1.5) {
					throw new LoadException();
				}
				if (moduleName.contentEquals("Buyers")) {
					status = titleComparision(authorityTitle, title);
					if (status == false) {
						System.err.println("Link is expired due to TITLE NOT MATCHED" + link);
						status = false;
						node.setExpiredReason("Authority title & title does not match");
						node.setExpiredStatus(status);
						return node;
					}
				}

				if (load.getLoadAvg() >= 1.5) {
					throw new LoadException();
				}
				// Link redirect comparisions. Comparision is done irrespective
				// of protocol
				if (link.replaceAll("https?://", "").equals(redirectedLink.replaceAll("https?://", ""))) {
					node.setExpiredReason("Link redirected so false");
					node.setExpiredStatus(status);
					return node;
				}

				if (load.getLoadAvg() >= 1.5) {
					throw new LoadException();
				}
				// Comparision done based on depth of link. If depth is
				// different for two links then it is expired.
				int depth1 = getdepth(link);
				int depth2 = getdepth(redirectedLink);

				/*
				 * if (depth1 == depth2) { node.setExpiredStatus("true"); return
				 * node; }
				 */
				if (load.getLoadAvg() >= 1.5) {
					throw new LoadException();
				}
				if (depth1 != depth2) {
					node.setExpiredReason("Depth of url and redirected url are not same");
					node.setExpiredStatus(status);
					return node;
				}

				if (load.getLoadAvg() >= 1.5) {
					throw new LoadException();
				}

				if (status == true) {
					node.setExpiredStatus(status);
					return node;
				}
			} else {
				status = true;
				node.setExpiredStatus(status);
				return node;
			}
		} catch (LoadException e) {
			System.exit(0);
		} catch (MongoSocketWriteException e) {
			System.out.println("Code exiting due to Net problem");
			System.exit(0);
		} catch (MalformedURLException e) {
			System.err.println(link + " is Malformed");
			status = false;
			node.setExpiredReason("Malformed URL");
			node.setExpiredStatus(status);
			return node;
		} catch (MongoSocketOpenException e) {
			System.out.println("Code exiting due to Net problem");
			System.exit(0);
		} catch (MongoException e) {
			System.out.println("Code is exiting due to net problem.");
			System.exit(0);
		} catch (UnknownHostException e) {
			status = true;
			node.setExpiredStatus(status);
			return node;
		} catch (NullPointerException e) {
			e.printStackTrace();
			System.exit(0);
			System.err.println("Null Pointer Exception " + link);
			status = false;
			node.setExpiredReason("Null Pointer Exception");
			node.setExpiredStatus(status);
			return node;
		} catch (HttpStatusException e) {
			System.err.println("Expired Link due to HttpStatus Exception::: " + link);
			e.printStackTrace();
			status = false;
			node.setExpiredReason("HTTP status Exception");
			node.setExpiredStatus(status);
			return node;
		} catch (UnsupportedMimeTypeException e) {
			status = true;
			node.setExpiredStatus(status);
			return node;
		} catch (Exception e) {
			System.out.println("Live Link:::" + link);
			System.out.println("Other Exception" + e.getMessage());
			status = true;
			node.setExpiredStatus(status);
			return node;
		}
		node.setExpiredStatus(true);
		return node;
	}

	public boolean titleComparision(String authorityTitle, String title) {
		// TODO Auto-generated method stub.

		title = title.trim();
		authorityTitle = authorityTitle.trim();
		if (authorityTitle.contentEquals(title)) {
			return true;
		}
		title = title.replaceAll("[^\\w\\s]", " ").replaceAll(" +", " ").trim().toLowerCase();
		authorityTitle = authorityTitle.replaceAll("[^\\w\\s]", " ").replaceAll(" +", " ").trim().toLowerCase();

		String[] dbtitle = title.split(" +");
		authorityTitle = " " + authorityTitle + " ";
		int cnt = 0;
		for (int i = 0; i < dbtitle.length; i++) {
			if (!authorityTitle.contains(" " + dbtitle[i] + " ")) {
				cnt++;
			}
		}

		if (cnt > 3) {
			return false;
		} else {
			return true;
		}
	}

	private boolean checkonStatus(int statusCode) {

		if (statusCode == 200) {
			return true;
		}
		return false;
	}

	public int getdepth(String url) {
		// TODO Auto-generated method stub
		int depth = 0;

		for (int i = 0; i < url.length(); i++) {
			char s = url.charAt(i);
			if (s == '/') {
				depth++;
			}
		}
		return depth;
	}

	public boolean expireMessage(Document doc) {
		if (doc.text().contains("Sorry this Ad is no longer available")
				|| doc.text().contains("This Job is no longer available")
				|| doc.text().contains("Sorry, that page doesn�t exist!") || doc.text().contains("job not found")
				|| doc.text().contains("job does not exist") || doc.text().contains("Job listing not found")
				|| doc.text().contains("The job listing you were looking for doesn�t seem to exist anymore.")
				|| doc.text().contains("The Job You Selected Has Expired")
				|| doc.text().contains("Sorry! That job has been removed")
				|| doc.text().contains("The Job You Selected Has Expired")
				|| doc.text().contains("Job listing not found")
				|| doc.text().contains("Sorry, we couldn't find the page you're looking for.")
				|| doc.text().contains("Sorry! This position is not active yet or has been expired.")
				|| doc.text().contains("This job has been closed") || doc.text().contains("Sorry. The COW says 404")
				|| doc.text().contains("We will be back soon")
				|| doc.text().contains("This page has been reserved for future use")
				|| doc.text().contains("Job expired"))

		{

			return false;

		} else {
			return true;
		}

	}

}